import Vue from 'vue'
import vueResource from 'vue-resource'
import Vuetify from 'vuetify'
import App from './App.vue'
import 'vuetify/dist/vuetify.min.css'
import Loading from '@/components/Loading'
import router from './router'
import Lodash from 'lodash'

Vue.use(vueResource);
Vue.use(Vuetify);
Vue.use(Lodash)

export const EventBus = new Vue();

Vue.config.productionTip = false

Vue.component('app-loading', Loading);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')